function generateOffer(nb) {
    return {
        id: "an_id" + nb,
        productId: "a_product_id" + nb,
        name: "a_name" + nb,
        price: 999.99,
        savingType: "$",
        media: [{
            url: "//an_url.com" + nb,
            size: "PDT40X40" + nb
        }]
    }
}

export function generateSingleOffer() {
    return generateOffer(1);
}

export function generateOffers(nb) {
    return [...Array(nb).keys()].map(i => generateOffer(i));
}

export function generateOffersAndFiltersFromServer() {
    return {
        data: {
            offers: [generateOffer(1)]
        }
    }
}