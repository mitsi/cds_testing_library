import React, {createContext, useContext, useState} from "react";
import {path} from "ramda";

const offerContext = createContext({});

export function useOfferProvider() {
    return useContext(offerContext);
}

const defaultState = [];

export function OfferProvider({children}) {
    const [offers, _setOffers] = useState(defaultState);

    function setOffers(offersFromServer) {
        const offers = path(["data", "offers"], offersFromServer) || defaultState;
        _setOffers(offers);
    }

    return <offerContext.Provider value={{offers, setOffers}}>
        {children}
    </offerContext.Provider>
}