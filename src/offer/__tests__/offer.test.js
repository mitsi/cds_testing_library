import {render} from "@testing-library/react";
import {generateSingleOffer} from "./fixtures";
import Offer from "../Offer";

test('Render an offer', () => {
    const {baseElement} = render(<Offer {...generateSingleOffer()} />);
    expect(baseElement).toMatchSnapshot();
});

test('Do not render if price is undefined', () => {
    const {container} = render(<Offer name={"aze"}/>);
    expect(container).toBeEmptyDOMElement();
});

test('Do not render if name is undefined', () => {
    const {container} = render(<Offer price={10}/>);
    expect(container).toBeEmptyDOMElement();
});

test('render with 0 as price', () => {
    const {baseElement} = render(<Offer name="a_thing" price={0}/>);
    expect(baseElement).toBeDefined();
});

test('Do not crash if media is undefined', () => {
    const {baseElement} = render(<Offer name={"a_thing"} price={999}/>);
    expect(baseElement).toBeDefined();
});


