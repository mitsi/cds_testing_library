import axios from "axios";

export default function useHttpOfferClient() {
    async function getOffers(searchword) {
        return axios.post('https://bffmobilesite.cdiscount.com/search?context=search', {
            searchWord: searchword,
            pageType: "SEARCH_AJAX",
            page: 1,
            perPage: 23,
            sort: "relevance",
            sortDir: "desc",
            filterIds: [],
            mediaSizes: [[140, 210], [140, 140], [300, 300]],
        });
    }

    return {getOffers}
};