import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/index';
import reportWebVitals from './reportWebVitals';
import {OfferProvider} from "./offer/OfferProvider";

import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <React.StrictMode>
        <OfferProvider>
            <App/>
        </OfferProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();