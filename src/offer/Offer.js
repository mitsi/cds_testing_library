import {Card} from "react-bootstrap";
import {OfferPropTypes} from "../propTypes";
import {path} from "ramda";

const emptyArray = [];

export default function Offer({media = emptyArray, name, price, savingType = "€"}) {
    if(!name || !price) return null;

    return (<Card style={{width: '18rem'}}>
        <Card.Img variant="top" src={path([0, "url"], media)}/>
        <Card.Body>
            <Card.Title>{price} {savingType}</Card.Title>
            <Card.Text>{name}</Card.Text>
        </Card.Body>
    </Card>);
}

Offer.propTypes = OfferPropTypes;