import {act, renderHook} from '@testing-library/react-hooks'
import '@testing-library/jest-dom'
import React from "react";
import {OfferProvider, useOfferProvider} from "../OfferProvider";
import {generateOffersAndFiltersFromServer, generateSingleOffer} from "./fixtures";

test("Should save data", () => {
    const wrapper = ({children}) => <OfferProvider>{children}</OfferProvider>
    const {result} = renderHook(() => useOfferProvider(), {wrapper})
    const offersFromServer = generateOffersAndFiltersFromServer();
    const expected = [generateSingleOffer()];

    act(() => {
        result.current.setOffers(offersFromServer);
    })

    expect(result.current.offers).toEqual(expected);
});

test("set default value if undefined", () => {
    const wrapper = ({children}) => <OfferProvider>{children}</OfferProvider>
    const {result} = renderHook(() => useOfferProvider(), {wrapper})
    const offersFromServer = undefined;
    const expected = [];

    act(() => {
        result.current.setOffers(offersFromServer);
    })

    expect(result.current.offers).toEqual(expected);
});