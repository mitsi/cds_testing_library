import {act, renderHook} from '@testing-library/react-hooks'
import '@testing-library/jest-dom'
import React from "react";
import useHttpOfferClient from "../UseHttpOfferClient";
import axios from "axios";

jest.mock("axios", () => ({
    __esModule: true,
    default: {
        post: jest.fn(),
    }
}))

describe("UseHttpOfferClient", () => {

    beforeEach(() => {
        axios.post.mockClear();
    });

    test("Should make request with searchWord", () => {
        axios.post.mockResolvedValue({});

        const {result} = renderHook(() => useHttpOfferClient())

        act(() => {
            result.current.getOffers("a_searchWord");
        });

        expect(axios.post).toHaveBeenCalledWith("https://bffmobilesite.cdiscount.com/search?context=search",
            {
                "filterIds": [],
                "mediaSizes": [[140, 210], [140, 140], [300, 300]],
                "page": 1,
                "pageType": "SEARCH_AJAX",
                "perPage": 23,
                "searchWord": "a_searchWord",
                "sort": "relevance",
                "sortDir": "desc"
            });
    });
});

