import App from "./App";
import {useOfferProvider} from "../offer/OfferProvider";
import useHttpOfferClient from "../offer/UseHttpOfferClient";
import {useEffect} from "react";

export const enhanced = WrappedComponent => () => {
    const {offers, setOffers} = useOfferProvider();
    const {getOffers} = useHttpOfferClient();
    useEffect(() => {
        getOffers("canapé").then(offers => {
            setOffers(offers);
        }).catch(exception => {
            console.error(exception);
            alert("ItIsPas fonctionnel");
        });
    }, []);

    return WrappedComponent({offers});
}

const enhancedApp = enhanced(App);
enhancedApp.displayName = 'App'
export default enhancedApp;