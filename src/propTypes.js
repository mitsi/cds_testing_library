import {arrayOf, exact, number, string} from "prop-types";

export const OfferPropTypes = {
    media: arrayOf(exact({
        url: string,
        size: string
    })),
    name: string,
    price: number,
    savingType: string
};

export const AppPropTypes = {
    offers: arrayOf(exact({
        ...OfferPropTypes,
        id: string,
        productId: string,
    }))
}