import {render} from "@testing-library/react";
import {useOfferProvider} from "../../offer/OfferProvider";
import {enhanced} from "../index";
import React from "react";
import {generateSingleOffer} from "../../offer/__tests__/fixtures";

jest.mock('../../offer/OfferProvider', () => ({
    useOfferProvider: jest.fn()
}));

const mockGetOffers = jest.fn()
jest.mock('../../offer/UseHttpOfferClient', () => ({
    __esModule: true,
    default: () => ({
        getOffers: mockGetOffers
    })
}));

describe("hoc app", () => {
    it('should request on instanciation', done => {
        const expected = generateSingleOffer();
        const setOffersMock = jest.fn(() => {
            expect(setOffersMock).toHaveBeenCalledWith(expected);
            expect(mockGetOffers).toHaveBeenCalledWith("canapé");
            done();
        });
        useOfferProvider.mockImplementation(() => ({offers: [], setOffers: setOffersMock}));
        mockGetOffers.mockResolvedValue(expected);

        const sink = () => <div/>
        const EnhancedSink = enhanced(sink);

        render(<EnhancedSink/>);
    });

    it('should alert on fail', done => {
        const setOffersMock = jest.fn();
        window.alert = jest.fn((text) => {
            expect(text).toBe("ItIsPas fonctionnel");
            expect(setOffersMock).not.toHaveBeenCalled();
            done();
        });

        useOfferProvider.mockImplementation(() => ({offers: [], setOffers: setOffersMock}));
        mockGetOffers.mockRejectedValue(null);

        const sink = () => <div/>
        const EnhancedSink = enhanced(sink);

        render(<EnhancedSink/>);
    });
});