import Offer from "../offer/Offer";
import {Col, Container, Navbar, Row} from "react-bootstrap";
import {AppPropTypes} from "../propTypes";


function App({offers = []}) {
    return <>
        <header>
            <Navbar expand="lg" variant="light" bg="light">
                <Navbar.Brand href="#">IT IS PAS CHER</Navbar.Brand>
            </Navbar>
        </header>
        <Container fluid>
            <Row className="justify-content-center">
                {offers.map(offer =>
                    <Col style={{flexGrow: 0, paddingBottom: 10}}
                         key={`${offer.id}-${offer.productId}`}>
                        <Offer
                            media={offer.media}
                            name={offer.name}
                            price={offer.price}
                            savingType={offer.savingType}
                        />
                    </Col>)}
            </Row>
        </Container>
    </>;
}

App.propTypes = AppPropTypes;
export default App;
