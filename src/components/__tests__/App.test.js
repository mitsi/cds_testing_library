import {render} from '@testing-library/react';
import App from '../App';
import {generateOffers} from "../../offer/__tests__/fixtures";

test('No Products', () => {
  const {baseElement} = render(<App />);
  expect(baseElement).toMatchSnapshot();
});

test('With Products', () => {
  const {baseElement} = render(<App offers={generateOffers(1)} />);
  expect(baseElement).toMatchSnapshot();
});

